<?php

require_once 'common-backend.php';

class Sqlite_backend extends Common_backend
{
    public $pdo = null;
    protected $dsn;
    protected $username;
    protected $pwd;

    public static $error = array (
        'duplicate' => 23000
    );

    public function
    __construct ($dbname)
    {
        $this->dsn = "sqlite:$dbname";
    }

    public function
    getPDO ()
    {
        $this->pdo = new PDO ($this->dsn);
        $this->pdo->query ('PRAGMA foreign_keys = ON');
        $this->pdo->setAttribute (PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $this->pdo;
    }

    public function
    last_insert_id ($class)
    {
        return $this->pdo->lastInsertId();
    }

    protected function
    get_primary_type ()
    {
        return 'INTEGER PRIMARY KEY AUTOINCREMENT';
    }
}

