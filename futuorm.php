<?php /*

Futuorm - Object-relational manager for PHP.
Copyright (c) 2010
    Viktor Semykin <thesame.ml@gmail.com>
    Sergey Kostenko <kostenko.ml@gmail.com>

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source

*/

/**
 * @file futuorm.php
 * @brief Main file
 *
 * Main Futuorm file to be included to client code
 */

if (defined ('DEBUG'))
    require_once ('debug.php');

/**
 * \private
 * Internal class. Represents every database record. Not for direct inheritance
 * or instantiating. Use \a Model instead.
 * @see Model
 */
class Base
{
    private static $anns = array ();

    /**
     * \brief   Internal method.
     *          Checks whether \a $ann is valid annotation.
     */
    public static function
    is_annotation ($ann)
    {
        return is_array ($ann) &&
               isset ($ann[0]) &&
               ($ann[0] != 'Set') &&
               ($ann[0] != '__futuorm_skip');
    }

    /**
     * \brief   Internal method.
     *          Returns all annotations of \a $class
     * @param class Class name or object instance
     * @see find_anns
     */
    public static function
    find_anns_of ($class)
    {
        if (is_object ($class))
            $class = get_class ($class);

        if (!isset (self::$anns[$class]))
        {
            $vars = get_class_vars ($class);
            $anns = array ();
            foreach ($vars as $field => $var)
            {
                if (!self::is_annotation ($var))
                    continue;
                $anns[$field] = $var;
            }
            self::$anns[$class] = $anns;
        }
        return self::$anns[$class];
    }

    /**
     * \brief   Internal method.
     *          Returns all annotations.
     * @see find_anns_of
     */
    public function
    find_anns ()
    {
        return Base::find_anns_of ($this);
    }

    /**
     * \brief   Internal method.
     *          Returns set containing this object alone.
     * @return ObjectSet containing this object alone.
     */
    protected function
    own_set ()
    {
        return ObjectSet::all (get_class ($this))->filter ('id', '=', $this->id);
    }

    /**
     * \brief Returns backward set of objects of class \a $class referenced by field name \a $field
     *
     * @param $class Name of referencing class
     * @param $field Name of referencing field
     */
    public function
    get_set_by_field ($class, $field)
    {
        return $this->own_set()->get_set_by_field ($class, $field);
    }

    public function
    __get ($field)
    {
        if (substr ($field, -4) == '_set')
            return $this->own_set()->get_set (substr ($field, 0, -4), $this);

        $lazy = $field.'_lazy';
        if (isset ($this->$lazy))
        {
            $this->$field = $this->$lazy->one;
            unset ($this->$lazy);
            return $this->$field;
        }
    }

    /**
     * \brief Creates instance of object.
     * @param args Array of initial values
     */
    public function
    __construct ($args = null)
    {
        foreach ($this->find_anns() as $field => $ann)
            $this->$field = isset ($args[$field]) ?
                $args[$field] :
                (isset ($ann['default']) ? $ann['default'] : null);
        $this->id = isset ($args['id']) ? $args['id'] : null;
    }

    /**
     * \brief Internal method.
     */
    public function
    __call ($method, $args)
    {
        $field = substr ($method, 0, -4);
        $rinst = $args[0];

        $vars = get_class_vars (get_class ($this));
        if (isset ($vars[$field]) && $vars[$field][0] == 'Set')
        {
            $rclass = $vars[$field]['of'];
            $middle = Helper::get_m2m_class(get_class ($this), $rclass, $field);
            if (substr ($method, -4) == '_add')
            {
                $obj = new $middle ();
                $obj->from = $this;
                $obj->to = $rinst;
                return $obj->save();
            }

            if (substr ($method, -4) == '_has')
            {
                return count (ObjectSet::all($middle)
                    ->filter (array ('from' => $this, 'to' => $rinst))
                    ->slice (0,1)) > 0;
            }

            if (substr ($method, -4) == '_del')
            {
                return ObjectSet::all($middle)
                    ->filter (array ('from' => $this, 'to' => $rinst))
                    ->delete();
            }
        }
        trigger_error ('Unknown method '.$method, E_USER_ERROR);
    }

    /**
     * \brief Saves object to database.
     */
    public function
    save ()
    {
        return ObjectSet::save_object ($this);
    }

    /**
     * \brief Saves object and all referenced objects
     */
    public function
    save_all ()
    {
        return ObjectSet::save_object ($this, true);
    }

    /**
     * \brief Deletes object from database.
     * @see ObjectSet::delete
     */
    public function
    delete ()
    {
        return $this->own_set()->delete ();
    }

    /**
     * \brief Sets properties of object.
     *
     * All values in \a $values with keys not in \a $allowed will be ignored.
     * @param values Array of values to be set
     * @param allowed Array of keys to be saved.
     */
    public function
    update ($values, $allowed = null)
    {
        foreach ($values as $key => $value)
        {
            if ($allowed === null || in_array ($key, $allowed))
                $this->$key = $value;
        }
    }
}

/**
 * \private
 * Class for implicit many-to-many classes. Not for direct instantiating.
 */
abstract class BaseM2M extends Base
{

}

/**
 * \brief Class representing all user models.
 */
abstract class Model extends Base
{

}

/**
 * \private
 * \brief Internal helper class.
 */
class Helper
{

    /**
     * Internal class. Returns name of class for many-to-many relation
     * between classes \a $from and \a $to by field \a $field. Creates
     * a class if one doesn't exist.
     * @param from Name of class containing m2m definition
     * @param to Name of referenced class
     * @param field Name of field containing m2m definition
     */
    public static function
    get_m2m_class ($from, $to, $field='')
    {
        $class = "{$from}_{$field}_{$to}";
        if (!Helper::class_exists ($class))
        {
            $code = <<<CODE
class {$from}_{$field}_{$to} extends BaseM2M
{
    public \$from = array ('Reference', 'to'=>'$from', 'required');
    public \$to = array ('Reference', 'to'=>'$to', 'required');
}
CODE;
            eval ($code);
        }
        return $class;
    }

    /**
     * Internal method. Finds annotation of field in class.
     * @param class_name Name of class
     * @param field Name of field in \a $class
     */
    public static function
    get_annotation ($class_name, $field)
    {
        if ($field == 'id')
            return 'Integer';
        $vars = get_class_vars ($class_name);
        return $vars[$field];
    }

    public static function
    class_exists( $classname )
    {
       return (class_exists ($classname) && in_array ($classname, get_declared_classes()));
    }
}

/**
 * Handles sets of objects in database. Implements Iterator and Countable
 * so you can use objects of this class in \a foreach and \a count(). Immutable.
 * @see Samples
 */
class ObjectSet implements Iterator, Countable
{
    protected $clause; /**< current WHERE clause */
    protected $class; /**< object class name*/
    protected $offset = 0; /**< current result offset (for LIMIT OFFSET)*/
    protected $count = -1; /**< cached value of count of records in result */
    protected $order = ''; /**< record ordering in result*/
    protected $reverse; /**< flag for reverse ordering result*/
    public static $pdo; /**< PDO instance to be used for DB queries */
    public static $backend;

    private $statement;
    private $row;
    private $query;
    private static $table_counter = 1;
    protected $presets = array ();
    protected $object_count = null;
    protected $joins = '';
    protected $byid = null;
    protected $clause_field = null;
    protected $alias = '';
    protected $prefetch = array ();
    protected $cached_sets = array ();

    public static $error;
    public static $err_duplicate;
    
    //DB-agnostic way to define true and false
    protected static $SQLTRUE = '1=1';
    protected static $SQLFALSE = '1=0';

    public static function
    init ($backend)
    {
        //FIXME: make it stateless
        self::$backend = $backend;
        self::$pdo = $backend->getPDO();
        self::$error = (object)($backend::$error);
        self::$err_duplicate = $backend::$error['duplicate'];
    }

    public static function
    release ()
    {
        self::$pdo = null;
        self::$backend->release();
    }

    public static function
    setup ()
    {
        $setup = self::$backend->setup();
        foreach ($setup as $st)
        {
//            echo $st.";\n";
            self::$pdo->query ($st);
        }
    }

    protected function
    __construct ($class)
    {
        $this->class = $class;
        $this->alias = self::get_alias();
    }

    /**
     * \private
     */
    public function
    preset ($field, $value)
    {
        $this->presets[$field] = $value;
    }

    /**
     * Creates copy of ObjectSet
     */
    protected function
    create_similar ()
    {
        $res = new ObjectSet ($this->class);
        foreach (array ('clause', 'offset', 'count', 'order', 'prefetch', 'joins', 'alias', 'byid', 'clause_field') as $field)
            $res->$field = $this->$field;
        return $res;
    }

    public function
    operate ($field, $operator, $value, $condition, $rfield = null)
    {
        if (is_array ($field) && $operator === null && $value === null)
        {
            $set = $this;
            foreach ($field as $key => $val)
                $set = $set->operate ($key, '=', $val, $condition, $rfield);
            return $set;
        }

        $res = $this->create_similar ();

        if (is_string ($field) && $operator === null && $value === null) //raw condition
        {
            $clause = $field;
        }
        else
        {
            $value = self::sql_value (
                Helper::get_annotation ($this->class, $field),
                $value,
                $rfield);
            $clause = "\"{$this->alias}\".\"$field\" $operator $value";
            if ($this->clause_field === null)
                $res->clause_field = $field;
            if ($this->clause_field && ($this->clause_field != $field))
                $res->clause_field = false;
//            var_dump ($this);
        }

        if (($res->clause == self::$SQLTRUE && $condition == 'AND') ||
            ($res->clause == self::$SQLFALSE && $condition == 'OR') ||
            ($res->clause == ''))
        {
            $res->clause = $clause;
            if ($field === 'id' && $operator === '=')
                $res->byid = $value;
        }
        else
        {
            $res->clause = "({$res->clause}) $condition ($clause)";
        }
        return $res;
    }

    /**
     * Creates new ObjectSet by filtering existing ObjectSet by condition
     * @param field Name of filtering field or \a array of filters.
     * @param operator SQL operator of condition
     * @param value value to be applied in condition
     * @returns Filtered ObjectSet
     * @see Samples
     */
    public function
    filter ($field, $operator=null, $value=null, $rfield = null)
    {
        return $this->operate ($field, $operator, $value, 'AND', $rfield);
    }

    /**
     * Creates new ObjectSet by addint object by condition to ObjectSet
     * @param field Name of filtering field or \a array of filters.
     * @param operator SQL operator of condition
     * @param value value to be applied in condition
     * @returns new ObjectSet
     * @see Samples
     */
    public function
    add ($field, $operator=null, $value=null, $rfield = null)
    {
        return $this->operate ($field, $operator, $value, 'OR', $rfield);
    }

    /**
     * Fetching a part of set
     * @param offset Offset of first element in original ObjectSet
     * @param count Maximal count of elements in new ObjectSet
     * @returns new ObjectSet
     * @see Samples
     */
    public function
    slice ($offset, $count)
    {
        $set = $this->create_similar();

        $set->offset += $offset;
        if ($this->count == -1)
            $set->count = $count;
        else
        {
            $set->count -= $offset;
            $set->count = min ($count, $set->count);
        }
        return $set;
    }

    /**
     * Sorts ObjectSet by field
     * @param field Name of field to be sorted by
     * @param reverse Flag indicating direction of sorting
     * @returns new ObjectSet
     * @see Samples
     */
    public function
    sortby ($field, $reverse=false)
    {
        $field = addslashes ($field); //Is it needed?
        $res = $this->create_similar();
        $res->order = " ORDER BY \"$field\"".($reverse ? ' DESC' : '');
        return $res;
    }

    public function
    force_prefetch ($class)
    {
        $set = $this->create_similar();
        $set->prefetch[] = $class;
        return $set;
    }

    /**
     * Creates full set of objects of \a $class
     * @param class Name of class
     * @returns new full ObjectSet
     */
    public static function
    all ($class)
    {
        $res = new ObjectSet ($class);
        $res->clause = self::$SQLTRUE;
        return $res;
    }

    /**
     * Creates empty set of objects of a \a $class
     * @param class Name of class
     * @returns new empty ObjectSet
     */
    public static function
    none ($class)
    {
        $res = new ObjectSet ($class);
        $res->clause = self::$SQLFALSE;
        return $res;
    }

    /**
      * Short circuit for fetching object by id
      * @param class Name of class
      * @param id Object id
      * @returns Object of class \a $class with \a $id
      */
    public static function
    byId ($class, $id)
    {
        $id = intval ($id);
        $res = new ObjectSet ($class);
        $res->clause = "\"{$res->alias}\".\"id\" = $id";
        return $res->one;
    }

    public function
    get_id_set ($rfield = null)
    {
        if (empty ($rfield))
            $rfield = 'id';
        //var_dump ($this->class);
        //var_dump ($this->alias);
        //var_dump ($this->joins);
        //var_dump ($this->clause);
        return ("SELECT \"{$this->alias}\".\"$rfield\" \nFROM \"{$this->class}\" as \"{$this->alias}\" {$this->joins} WHERE {$this->clause}".$this->get_limit());
    }

    public function
    get_alias ()
    {
        #return 't_'.$this->table_counter++;
        return 't_'.self::$table_counter++;
    }

    public function
    field_list ($class, $alias, $prefix, &$tables, $prefetch = true)
    {
        $fields = '';
        if (!is_subclass_of ($class, 'BaseM2M'))
            $fields = "\n\t\"$alias\".\"id\" as \"{$prefix}id\", ";
        foreach (Base::find_anns_of ($class) as $field => $ann)
        {
            if (isset ($this->presets[$field]))
                continue;
            if ($ann[0] == 'Reference' && 
                (in_array ('prefetch', $ann) || in_array ($field, $this->prefetch)) &&
                $prefetch)
            {
                $a2 = $this->get_alias();
                $tables .= "\n\tLEFT JOIN \"{$ann['to']}\" as \"$a2\" ON \"$alias\".\"$field\" = \"$a2\".\"id\"";
                $fields .= $this->field_list ($ann['to'], $a2, $field.'_', $tables, false);
            }
            else
                $fields .= "\n\t\"$alias\".\"$field\" as \"{$prefix}{$field}\", ";
        }
        return $fields;
    }

    protected function
    get_limit ()
    {
        $r = '';
        if ($this->count != -1)
        {
            $r = "\nLIMIT {$this->count}";
            if ($this->offset != 0)
                $r .= " OFFSET {$this->offset}";
        }
        return $r;
    }

    public function
    sql_query ()
    {
        if ($this->query === null)
        {
            $query = 'SELECT ';

            $tables = "\"{$this->class}\" as \"{$this->alias}\"";
            $fields = $this->field_list ($this->class, $this->alias, '', $tables);
            $tables .= $this->joins;
            $query .= $fields;
            $query = substr ($query, 0, -2);
            $query .= "\nFROM $tables\nWHERE ".
                (empty ($this->clause) ? '1' : $this->clause).
                $this->order.
                $this->get_limit();
            $this->query = $query;
        }
        return $this->query;
    }

    public function
    count_query ()
    {
        $f = "\"{$this->alias}\".\"id\"";
        if (is_subclass_of ($this->class, 'BaseM2M'))
            $f = '"from"';
        $query = "SELECT COUNT(*) FROM (\n\t".
            "SELECT $f FROM \"{$this->class}\" as \"{$this->alias}\"";

        $query .= "{$this->joins} WHERE {$this->clause}".
            $this->get_limit().') as "stub_alias"';
        return $query;
    }

    /**
     * Internal method. Executes query \a $q on self::$pdo
     * @param q SQL query
     * @returns PDOStatement
     */
    public static function
    do_query ($q, $show = false)
    {
        if ($show)
            echo $q.";\n\n";
        if (defined ('DEBUG') && DEBUG)
        {
            $trace = debug_backtrace ();
            foreach ($trace as $line)
            {
                $dir = dirname (__FILE__);
                if (isset ($line['file']) && (dirname ($line['file']) != $dir))
                {
                    $path = @$line['file'];
                    for ($i = 1; substr_compare ($path, __DIR__, 0, $i) == 0; $i++);
                    $path = substr ($path, $i-strlen ($path)-1);
        //            FODebug::println ('('.@$line['class'].'::'.@$line['function'].")\t".$path.':'.@$line['line']);
                }
            }
            $start = microtime (true);
            FODebug::println ($q);
            $res = self::$pdo->query ($q);
            $s = sprintf ("%.3f %s\n", ((microtime (true) - $start)*1000), $q);
    //        FODebug::println ($s);
            if ($res === false)
            {
                echo '<pre>'.FODebug::get().'</pre>';
                var_dump (self::$pdo->errorInfo());
                die();
            }
            return $res;
        }
        else
        {
            //fwrite (STDOUT, "$q\n");
            return self::$pdo->query ($q);
        }
    }

    /**
     * Saves object to database. If cascade is true, saves referenced objects too
     * @param obj Object to save
     * @param cascade Whether referenced objects are to be saved
     * @returns Primary key (id) of saved object
     */
    public static function
    save_object ($obj, $cascade = false)
    {
        if ($cascade)
        {
            foreach (Base::find_anns_of ($obj) as $field => $ann)
                if (($ann[0] == 'Reference') && ($obj->$field !== null))
                    self::save_object ($obj->$field, true);
        }

        if ($obj->id == null) //insert
        {
            $fields = '';
            $values = '';
            foreach (Base::find_anns_of ($obj) as $field => $ann)
            {
                $fields .= "\n\t\"$field\", ";
                $values .= self::sql_value ($ann, $obj->$field, null, true).', ';
            }
            $fields = substr ($fields, 0, -2);
            $values = substr ($values, 0, -2);
            $q = 'INSERT INTO "'.get_class ($obj)."\" ($fields) VALUES ($values)";
            self::do_query($q);
            if (!($obj instanceof BaseM2M))
                $obj->id = self::$backend->last_insert_id (get_class ($obj));
            //$obj->id = self::$pdo->lastInsertId();
        }
        else //update
        {
            $q = 'UPDATE "'.get_class ($obj).'" SET ';
            foreach (Base::find_anns_of ($obj) as $field => $ann)
                $q .= "\"$field\" = ".Helper::sql_value ($ann, $obj->$field).', ';
            $q = substr ($q, 0, -2);
            $q .= ' WHERE "id" = '.$obj->id;
            self::do_query ($q);
        }
    }

    public function
    m2m_set ($class, $middle, $c1, $c2)
    {
        $a2 = self::get_alias();
        $set = new ObjectSet ($class);
        if ($this->byid)
        {
            $join = "\n\tJOIN \"{$middle}\" as \"{$a2}\" ON \"{$a2}\".\"{$c1}\" = \"{$set->alias}\".\"id\"";
            $set->joins = $join.$this->joins;
            $set->clause = "\"{$a2}\".\"{$c2}\" = {$this->byid}";
        }
        elseif ($this->clause == self::$SQLTRUE)
        {
            $join = "\n\tJOIN \"{$middle}\" as \"{$a2}\" ON \"{$a2}\".\"{$c1}\" = \"{$set->alias}\".\"id\"";
            $set->joins = $join.$this->joins;
            $set->clause = self::$SQLTRUE;
        }
        else
        {
            $join = "\n\tJOIN \"{$middle}\" as \"{$a2}\" ON \"{$a2}\".\"{$c1}\" = \"{$set->alias}\".\"id\"";
            $join .= "\n\tJOIN \"{$this->class}\" as \"{$this->alias}\" ON \"{$a2}\".\"{$c2}\" = \"{$this->alias}\".\"id\"";
            $set->joins = $join.$this->joins;
            $set->clause = $this->clause;
        }
        return $set;
    }

    public function
    ref_set ($class, $field, $rev = false)
    {
        $set = new ObjectSet ($class);
        if ($this->byid)
        {
            $set->clause = "\"{$set->alias}\".\"$field\" = {$this->byid}";
            $set->joins = $this->joins;
        }
        else
        {
            if ($rev)
                $join = "\n\tJOIN \"{$this->class}\" as \"{$this->alias}\" ON \"{$set->alias}\".\"id\" = \"{$this->alias}\".\"$field\"";
            else
                $join = "\n\tJOIN \"{$this->class}\" as \"{$this->alias}\" ON \"{$set->alias}\".\"$field\" = \"{$this->alias}\".\"id\"";
            $set->joins = $join.$this->joins;
            $set->clause = $this->clause;
        }
        return $set;
    }

    public function
    get_set_by_field ($class, $field)
    {
        if (isset ($this->cached_sets["$class.$field"]))
            $set = $this->cached_sets["$class.$field"];

        $vars = get_class_vars ($class);

        if (@$vars[$field][0] == 'Set')
        {
            $middle = Helper::get_m2m_class ($class, $this->class, $field);
            $set = $this->m2m_set ($class, $middle, 'from', 'to');
            return $set;
        }
        if (@$vars[$field][0] == 'Reference')
        {
            return $this->ref_set ($class, $field);
        }
    }

    public function
    get_set_uncached ($class)
    {
        //1st case (probably):
        if (Helper::class_exists ($class))
        {
            foreach (Base::find_anns_of ($class) as $field => $ann)
            {
                if (@$ann[0] == 'Reference' && @$ann['to'] == $this->class)
                {
                    $set = $this->ref_set ($class, $field);
                    return $set;
                }
            }
        }

        $vars = get_class_vars ($this->class);
        //2nd case:
        if (isset ($vars[$class]) && $vars[$class][0] == 'Set')
        {
            $field = $class;
            $class = $vars[$class]['of'];

            $middle = Helper::get_m2m_class ($this->class, $class, $field);
            $set = $this->m2m_set ($class, $middle, 'to', 'from');
            return $set;
        }

        //3rd case:
        if (Helper::class_exists ($class))
        {
            foreach (get_class_vars ($class) as $field => $ann)
            {
                if (@$ann[0] == 'Set' && @$ann['of'] == $this->class)
                {
                    $middle = Helper::get_m2m_class ($class, $this->class, $field);
                    $set = $this->m2m_set ($class, $middle, 'from', 'to');
                    return $set;
                }
            }
        }

        //4th case:
        if (isset ($vars[$class]) && @$vars[$class][0] == 'Reference')
        {
            $field = $class;
            $class = $vars[$class]['to'];
            $set = $this->ref_set ($class, $field, true);
            return $set;
        }
    }

    public function
    get_set ($class, $preset = null)
    {
        /**
         * Returns set of related objects. Order of search:
         * 1. Objects directly referencing this set by class name.
         * 2. Objects connected to this set thru m2m by field name.
         * 3. Objects connected to this set thru m2m by class name.
         * 4. Objects referenced by this by field name.
         */
        if (isset ($this->cached_sets[$class]))
            $set = $this->cached_sets[$class];

        $set = $this->get_set_uncached ($class);
        if ($preset)
            $set->presets[$class] = $preset;

        $this->cached_sets[$class] = $set;
        return $set;
    }

    public function
    __get ($field)
    {
        if ($field == 'one')
        {
            if (!$this->statement)
                $this->rewind();
            return $this->current();
        }

        if ($field == 'all')
        {
            $res = array ();
            foreach ($this as $inst)
                $res[] = $inst;
            return $res;
        }
        if (substr ($field, -4) == '_set')
            return $this->get_set (substr ($field, 0, -4));
    }

    public function
    __toString ()
    {
        return "<Set of {$this->class}>";
    }

    /**
     * Internal method. Returns value suitable for SQL-queries.
     * @param annotation Annotation defining property
     * @param value Raw value; Can be modified by this call
     * @param rfield For internal usage
     * @param creation For internal usage
     * @returns sql value
     */
    public static function
    sql_value ($annotation, &$value, $rfield = null, $creation = false)
    {
        $type = $annotation[0];

        if ($value instanceof ObjectSet)
            return '('.$value->get_id_set ($rfield).')';

        if ($type == 'Reference')
        {
            if (($value !== null) && !($value instanceof $annotation['to']) && !(is_int ($value)))
            {
                $given = is_object ($value) ? 'class '.get_class ($value) : gettype ($value);
                trigger_error ("Wrong reference; Class {$annotation['to']} required, $given given", E_USER_ERROR);
            }
            if (is_int ($value))
                return $value;
            return isset ($value->id) ? $value->id : 'NULL';
        }

        if ($type == 'DateTime')
        {
            if (in_array ('modification', $annotation) ||
               (in_array ('creation', $annotation) && $creation))
            {
                $value = time();
                return time();
            }
        }

        if ($value === null)
            return 'NULL';

        if ($type == 'Integer')
            return intval ($value);

        if ($type == 'Boolean')
            return $value ? '1' : '0';

        if ($type == 'DateTime')
            return intval ($value);

        if ($type == 'Float')
            return is_nan (floatval ($value)) ? 'NULL' : floatval ($value);

        if ($type == 'Text')
        {
            $res = (string)$value;
            if (in_array ('htmlsafe', $annotation))
                $res = htmlentities ($res);
            return self::$pdo->quote ($res);
        }
        if ($type == 'Raw')
            return self::$pdo->quote (serialize ($value));

        if (is_array ($value))
            return '(\''.implode ('\',\'', $value).'\')';
        return ObjectSet::$backend->sql_value ($annotation, $value, $rfield, $creation);
        //return '\''.addslashes ((string)$value).'\'';
    }

    protected function
    php_value ($ann, $val)
    {
        if ($ann[0] == 'Raw')
            return unserialize ($val);

        elseif ($ann[0] == 'Boolean')
            return $val ? true : false;

        elseif ($ann[0] == 'Choice')
            return $ann['choices'][$val];

        else
            return $val;
    }

    public function
    create_obj_of_array ($class, $vars, $prefix='')
    {
        if ($vars[$prefix.'id'] === null)
            return null;
        $obj = new $class();
        $obj->id = $vars[$prefix.'id'];
        foreach (Base::find_anns_of ($class) as $field => $ann)
        {
            if (isset ($this->presets[$field]))
            {
                $obj->$field = $this->presets[$field];
                continue;
            }
            if ($ann[0] == 'Reference')
            {
                if (in_array ('prefetch', $ann) || in_array ($ann['to'], $this->prefetch))
                {
                    $obj->$field = $this->create_obj_of_array ($ann['to'], $vars, $field.'_');
                }
                else
                {
                    if ($vars[$prefix.$field] === null)
                        $obj->$field = null;
                    else
                    {
                        $lazy_set = ObjectSet::all ($ann['to'])->filter ('id', '=', $vars[$prefix.$field]);
                        unset ($obj->$field);
                        $obj->{$field.'_lazy'} = $lazy_set;
                    }
                }
            }
            elseif ($ann[0] == 'Raw')
                $obj->$field = unserialize ($vars[$prefix.$field]);

            elseif ($ann[0] == 'Boolean')
                $obj->$field = $vars[$prefix.$field] ? true : false;

            else
                $obj->$field = $vars[$prefix.$field];
                //$obj->$field = $this->php_value ($ann, $vars[$prefix.$field]);
        }
        return $obj;
    }

    protected function
    dump_table ($table)
    {
        var_dump (self::$pdo->query ("SELECT * FROM $table")->fetchAll());
    }

    protected function
    delete_m2m ($midname, $field, $ids)
    {
        $query = "DELETE FROM \"$midname\" WHERE \"$field\" IN (".$ids.")\n";
//        echo $query."\n";
        $this->do_query ($query);
//        self::dump_table ($midname);
    }

    protected function
    warm_up()
    {
        static $already_done = false;
        if ($already_done)
            return;
        //creating needed implicit classes
        $classes = get_declared_classes();
        foreach ($classes as $class)
        {
            if (!is_subclass_of ($class, 'Model'))
                continue;
            $vars = get_class_vars ($class);
            foreach ($vars as $field => $ann)
            {
                if (@$ann[0] == 'Set' && @$ann['of'] == $this->class)
                    Helper::get_m2m_class ($class, $this->class, $field);
            }
        }
        $vars = get_class_vars ($this->class);
        foreach ($vars as $field => $ann)
        {
            if (@$ann[0] == 'Set')
                Helper::get_m2m_class ($this->class, $ann['of'], $field);
        }
    }

    /**
     * Deletes objects belonging to this set
     */
    public function
    delete ()
    {
        $this->warm_up();

        if (is_a ($this->class, 'BaseM2M', true))
        {
            //FIXME: this branch of code is total shame, it should be removed asap
            $clause = str_replace ($this->alias, $this->class, $this->clause);
            $this->do_query ("DELETE FROM \"{$this->class}\" WHERE $clause");
            return;
        }

        //Creating list of IDs to be deleted:
        if ($this->byid)
            $ids = $this->byid;
        else
        {
            $ids = $this->do_query ($this->get_id_set())->fetchAll (PDO::FETCH_COLUMN);
            $ids = implode (', ', $ids);
        }

//TODO: integrity checks for MyISAM
//TODO: cascade delete

        //Processing forward sets...
        foreach (get_class_vars ($this->class) as $field => $ann)
        {
            if (@$ann[0] != 'Set')
                continue;
            $mid = Helper::get_m2m_class ($this->class, $ann['of'], $field);
            self::delete_m2m ($mid, 'from', $ids);
        }
        //Processing backward sets...
        $classes = get_declared_classes();
        foreach ($classes as $class)
        {
            if (!is_subclass_of ($class, 'Model'))
                continue;
            $anns = get_class_vars ($class);
            foreach ($anns as $field => $ann)
            {
                if (@$ann[0] == 'Set' && @$ann['of'] == $this->class)
                {
                    $mid = Helper::get_m2m_class ($class, $this->class, $field);
                    self::delete_m2m ($mid, 'to', $ids);
                }
            }
        }
        $this->do_query ("DELETE FROM \"{$this->class}\" WHERE \"id\" IN (".$ids.")");
    }

    public function
    has (Model $inst)
    {
        if (get_class ($inst) != $this->class)
            return false;
        return count ($this->filter ('id', '=', $inst->id)->slice (0,1)) > 0;
    }

    /* Iterator interface */

    public function
    current ()
    {
        $row = $this->statement->fetch (PDO::FETCH_ASSOC);
        return $this->create_obj_of_array ($this->class, $row);
    }

    public function
    key ()
    {
        return $this->row;
    }

    public function
    next ()
    {
        $this->row++;
    }

    public function
    rewind ()
    {
        $this->statement = self::do_query ($this->sql_query ());
        $this->row = 0;
    }

    public function
    valid ()
    {
        return $this->row < $this->statement->rowCount();
    }

    /* Countable interface */
    public function
    count ()
    {
        if ($this->object_count === null)
        {
            if ($this->statement !== null)
                $this->object_count = $this->statement->rowCount();
            else
                $this->object_count = self::do_query ($this->count_query())->fetchColumn();
        }
        return $this->object_count;
    }

#    public function
#    diag ()
#    {
#        echo "\nObjectSet of {$this->class}\n";
#        var_dump ($this->alias);
#        var_dump ($this->joins);
#        var_dump ($this->clause);
#    }
}


