<html>
<head>
<style type="text/css">
.toctitle {
    padding-left: 15px;
    font-size: 200%;
}
.toc {
    position: absolute; 
    right: 0;
    top: 0;
    background: #f8f8ff;
    width: 300px;
    margin: 5px;
    font-family: 'Tahoma';
}
.toc > ul {
    list-style: none;
    padding-left: 0;
}
.toc > ul > li {
}
.toc > ul > li > a {
    padding: 2px 15px;
    display: block;
    background: #E9E9F0;
    margin: 5px 0;
    font-weight: bold;
}
ul {
    color: #aaaaaa;
    list-style-type: square; 
    list-style-position: inside;
    padding-left: 15px;
}
li {
    font-size: 98%;
}
body {
    padding-right: 320px;
    padding-left: 10px;
    font-family: 'PT Serif', 'Times New Roman';
}
a {
    color: #458;
}
h1,h2,h3,h4 {
    font-weight: normal;
    padding-left: 10px;
    font-family: 'Tahoma';
}
h1 {
    background: #E9E9F0;
}
h3,h4 {
    margin-bottom: 0;
}
p {
    margin-left: 10px;
    margin-top: 0;
}
pre {
    margin-left: 20px;
}
.logo h1 {
    display: inline;
    font-size: 4em;
    padding-left: 30px;
    background: inherit;
}
.logo img {
    margin: 10px;
}
</style>

<link rel="stylesheet" href="github.css"/>
<script src="highlight.pack.js"></script>
<script>hljs.initHighlightingOnLoad();</script>
</head>
<body>
<div class="logo">
    <img src="logo.png" align="top" />
    <h1>Futuorm</h1>
</div>
<?= $body ?>

<hr/>

<center>Copyright &copy; 2010-2012 Viktor Semykin

<br/>
<br/>
<small>
<a rel="license" href="http://creativecommons.org/licenses/by/3.0/">
    <img alt="Creative Commons License" style="border-width:0" src="http://i.creativecommons.org/l/by/3.0/80x15.png" />
    <br/>
</a>
<span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type">Futuorm documentation</span> by <span xmlns:cc="http://creativecommons.org/ns#" property="cc:attributionName">Viktor Semykin</span> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/3.0/">Creative Commons Attribution 3.0 Unported License</a>.
</small>
</center>
</body>
</html>
