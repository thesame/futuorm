<?php

require_once 'utils.php';

function
render ($template, $ctx = array())
{
    extract ($ctx);

    ob_start();
    include $template;
    $res = ob_get_contents();
    ob_end_clean();
    return $res;
}

function
build_toc_branch ($elm, $level)
{
    $name = $elm->attributes->getNamedItem ('name')->textContent;
    $section = $elm->attributes->getNamedItem ('section')->textContent;
    $anchor = $elm->attributes->getNamedItem ('anchor')->textContent;

    $res = sprintf ("%{$level}s * [%s#%s %s]\n", '', $section, $anchor, $name);

    if ($elm->hasChildNodes())
    {
        foreach ($elm->childNodes as $node)
            $res .= build_toc_branch ($node, $level+1);
    }
    return $res;
}

function
render_element_wiki ($el, $level)
{
    if ($el instanceof DOMText)
    {
        $text = $el->wholeText;
        if ($text == "\n\n")
            return "\n";
        return $text;
    }

    if ($el->hasChildNodes())
    {
        $inner = '';
        foreach ($el->childNodes as $node)
            $inner .= render_element_wiki ($node, $level+1);
    }

    $tag = $el->tagName;
    if ($tag == 'p')
        return "$inner\n";
    if ($tag == 'h1')
        return "= $inner =\n";
    if ($tag == 'h2')
        return "== $inner ==\n";
    if ($tag == 'h3')
        return "=== $inner ===\n";
    if ($tag == 'h4')
        return "==== $inner ====\n";
    if ($tag == 'em')
        return "*$inner*";
    if ($tag == 'pre')
        return "{{{\n$inner}}}\n";
    if ($tag == 'a')
    {
        $href = $el->attributes->getNamedItem ('href')->textContent;
        return "[$href $inner]";
    }
    if ($tag == 'hr')
        return "----\n";
    if (($tag == 'code') && ($el->parentNode->tagName != 'pre'))
        return "`$inner`";
    if (!@$inner)
        var_dump ($tag);
    return $inner;
}

function
build_page ($title)
{
    $dom = get_document ($title);
    $dom->formatOutput = true;
    $body = $dom->getElementsByTagName ('body')->item(0);
    $wiki = render_element_wiki ($body, 0)."\n";
    file_put_contents ("wiki/$title.wiki", $wiki);
}

function
build ()
{
    global $pages;

    $toc = get_toc ();
    $tocel = $toc->getElementsByTagName ('toc')->item (0);

    $wikitoc = '';
    foreach ($tocel->childNodes as $elm)
        $wikitoc .= build_toc_branch ($elm, 0);
    
    file_put_contents ('wiki/TableOfContents.wiki', $wikitoc);

    foreach ($pages as $page)
        build_page ($page);
}

function
build2 ()
{
    global $markdown, $md_args;

    $toc = '<div class="toc"><span class="toctitle">Contents</span>';
    foreach (array ('Overview', 'Models', 'ObjectSets', 'Relations', 'DatabaseSetup', 'Additional') as $section)
    {
        $src = file_get_contents ("markdown/$section.markdown")."\n";
        $html = `$markdown $md_args << "_EOF_"\n$src\n_EOF_\n`;
        $dom = new DOMDocument;
        $dom->loadHTML ($html);
        $prev = '';
        foreach ($dom->getElementsByTagName ('body')->item(0)->childNodes as $c)
        {
            $tag = @$c->tagName;
            if (!preg_match ('/^h([1234])$/', $tag, $m))
                continue;
            $lvl = $m[1];
            if ($lvl > $prev)
                $toc .= "<ul>\n";
            if ($lvl < $prev)
                $toc .= "</ul>\n";
            $prev = $lvl;
            $part = str_replace (' ', '_', $c->textContent);
            $toc .= "<li><a href='$section.html#$part'>{$c->textContent}</a></li>\n";
        }
        while ($prev--) $toc .= "</ul>";
    }
    $toc .= '</div>';

    foreach (array ('Overview', 'Models', 'ObjectSets', 'Relations', 'DatabaseSetup', 'Additional') as $section)
    {
        $md = file_get_contents ("markdown/$section.markdown")."\n";
        $html = `$markdown $md_args << "_EOF_"\n$md\n_EOF_\n`;
        $dom = new DOMDocument;
        $dom->loadHTML ($html);
        foreach ($dom->getElementsByTagName ('body')->item(0)->childNodes as $c)
        {
            $tag = @$c->tagName;
            if (!preg_match ('/^h([1234])$/', $tag, $m))
                continue;
            $subel = $c->childNodes->item(0);
            $title = strip_tags ($dom->saveHTML ($subel));
            $name = str_replace (' ', '_', $title);
            $c->removeChild ($subel);
            $anchor = $dom->createElement ('a');
            $anchor->setAttribute ('href', "#$name");
            $anchor->setAttribute ('name', $name);
            $anchor->appendChild ($subel);
            $c->appendChild ($anchor);
        }
        $html2 = $dom->saveHTML ();
        $f = render ('template.php', array (
            'title' => $section,
            'body' => $toc.$html2
        ));
        file_put_contents ("$section.html", $f);
    }

}

build();

