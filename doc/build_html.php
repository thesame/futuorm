<?php

require_once 'utils.php';

function
render ($template, $ctx = array())
{
    extract ($ctx);

    ob_start();
    include $template;
    $res = ob_get_contents();
    ob_end_clean();
    return $res;
}

function
build_toc_branch ($dom, $elm, $level)
{
    $item = $dom->createElement ('li');
    $name = $elm->attributes->getNamedItem ('name')->textContent;
    $section = $elm->attributes->getNamedItem ('section')->textContent;
    $anchor = $elm->attributes->getNamedItem ('anchor')->textContent;
    $a = $dom->createElement ('a', $name);
    $a->setAttribute ('href', "$section.html#$anchor");
    $item->appendChild ($a);

    if ($elm->hasChildNodes())
    {
        $ul = $dom->createElement ('ul');
        foreach ($elm->childNodes as $node)
            $ul->appendChild (build_toc_branch ($dom, $node, $level+1));
        $item->appendChild ($ul);
    }
    return $item;
}

function
build ()
{
    global $pages;

    $toc = get_toc ();
    $tocel = $toc->getElementsByTagName ('toc')->item (0);

    $dom = new DOMDocument;
    $ul = $dom->createElement ('ul');
    $dom->appendChild ($ul);
    foreach ($tocel->childNodes as $elm)
    {
        $item = build_toc_branch ($dom, $elm, 0);
        $ul->appendChild ($item);
    }
    $dom->formatOutput = true;
    $htmltoc = ' <div class="toc"><span class="toctitle">Contents</span>'.$dom->saveHTML().'</div>';

    foreach ($pages as $page)
    {
        $dom = get_document ($page);
        foreach ($dom->getElementsByTagName ('body')->item(0)->childNodes as $c)
        {
            $tag = @$c->tagName;
            if (!preg_match ('/^h([1234])$/', $tag, $m))
                continue;
            $subel = $c->childNodes->item(0);
            $title = strip_tags ($dom->saveHTML ($subel));
            $name = str_replace (' ', '_', $title);
            $c->removeChild ($subel);
            $anchor = $dom->createElement ('a');
            $anchor->setAttribute ('href', "#$name");
            $anchor->setAttribute ('name', $name);
            $anchor->appendChild ($subel);
            $c->appendChild ($anchor);
        }

        $dom->formatOutput = true;
        $html = render ('template.php', array (
            'title' => $page,
            'body' => $htmltoc.' '.$dom->saveHTML()
        ));
        file_put_contents ("html/$page.html", $html);
    }

    copy ('html/Overview.html', 'html/index.html');
}

build();

