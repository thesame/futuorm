<?php

$pages = ['Overview', 'Models', 'ObjectSets', 'Relations', 'DatabaseSetup', 'Additional'];

function
do_markdown ($src)
{
    require_once 'php-markdown/markdown.php';

    return Markdown ($src);
}

function
get_document ($title = null)
{
    global $markdown, $md_args, $pages;
    $text = '';
    if ($title)
        $text = file_get_contents ("markdown/$title.markdown")."\n";
    else
        foreach ($pages as $page)
            $text .= file_get_contents ("arkdown/$page.markdown")."\n";
    $html = do_markdown ($text);
    $doc = new DOMDocument;
    $doc->loadHTML ($html);
    return $doc;
}

function
get_toc ()
{
    global $pages;

    $toc = new DOMDocument;
    $cur = $toc->createElement ('toc');
    $parent = $cur;
    $parent_lvl = 0;
    $toc->appendChild ($cur);

    foreach ($pages as $page)
    {
        $dom = get_document ($page);

        foreach ($dom->getElementsByTagName ('body')->item(0)->childNodes as $c)
        {
            $tag = @$c->tagName;
            if (!preg_match ('/^h([1234])$/', $tag, $m))
                continue;
            $lvl = $m[1];
            $part = str_replace (' ', '_', $c->textContent);
            $item = $toc->createElement ('item');
            $item->setAttribute ('name', $c->textContent);
            $item->setAttribute ('anchor', $part);
            $item->setAttribute ('section', $page);

            while ($lvl < $parent_lvl+1)
            {
                $parent = $parent->parentNode;
                $parent_lvl--;
            }

            $parent->appendChild ($item);
            $parent = $item;
            $parent_lvl = $lvl;
        }
    }
    return $toc;
}
