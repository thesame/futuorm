Object sets
===========

Having objects stored in database is not much useful unless they can be
retreived. Futuorm introduces object sets for this purpose. Each object set is
represented by object of `ObjectSet` class.  See following examples.

Creating
--------
`ObjectSet` class cannot be created directly from constructor, instead it has
two fabric methods: [all](#all) returning full set and [none](#none) returning empty set.
Note that object sets are immutable.  It means that instance of `ObjectSet`
class cannot be modified, but it can be used as template for new set.

### `all`
`all` method returns new `ObjectSet` containing full set of objects of given class. E.g.:

    $users = ObjectSet::all ('User');

will return set of _all_ users existing in table `User`.

### `none`
`none` method returns new `ObjectSet` containing empty set of objects of given class. E.g.:

    $users = ObjectSet::none ('User');
        
will return empty set of `User` objects. This implies `count ($users) === 0`.

### `byId`
`byId` method is shorthand method for fetching single object by its `id`.

    ObjectSet::byId ('Document', 1);

is fully equivalent to

    ObjectSet::all ('Document')->filter ('id', '=', 1)->one;

(see [one](#one))

Fetching objects
----------------

### iterating
`ObjectSet` class supports
[Iterator](http://php.net/manual/en/class.iterator.php) interface, so it can be
simply iterated using `foreach`:

    foreach (ObjectSet::all ('Documents') as $document)
        output ($document); //hypothetical function

### `one`
`one` is pseudo-attribute of `ObjectSet` class returning next object in set. You
can call it sequentially to fetch objects one by one:

    $documents = ObjectSet::all ('Documents');
    output_first ($documents->one); //returns first document from set
    output_second ($documents->one); //returns second document from set

Modifiying
----------
Objects of `ObjectSet` class are immutable. If you want to select objects
different than current object set, you need to create new `ObjectSet` based on
existing one. There are bunch of methods returning new modified `ObjectSet`:

### `filter`
    public function ObjectSet::filter ($field, $operator=null, $value=null, $rfield = null);

`filter` method returns new `ObjectSet` of objects from existing set filtered by criteria. Objects not meeting
criteria are excluded from set. Simple usecase is:

    ObjectSet::all ('Document')->filter ('name', '=', 'homepage');

This construction means "Select all documents named 'homepage'".
In this example we create two object sets:

1.  `ObjectSet::all ('Document')`: full set of `Document` objects.
2.  `->filter ('name', '=', 'homepage')`: object set based on full set filtered by `name = 'homepage'` criteria.

As long as `filter` is method of `ObjectSet` returning new `ObjectSet`,
`filter`s can be chained to achieve necessary object set:

    ObjectSet::all ('Document')->filter ('name', '=', 'homepage')->filter ('type', '=', 'html');
    //Note that this chain can be simplified. See next sample.

`filter` method accepts different forms of criterias:

    //1. property, operation, value
    ObjectSet::all ('Document')->filter ('name', '=', 'homepage');
    ObjectSet::all ('Document')->filter ('type', 'IN', ['html', 'pdf']);

    //2. array of property => value
    ObjectSet::all ('Document')->filter ([
        'name' => 'homepage',
        'type' => 'html'
    ]);

The latter form is total equivalent of chaining two filters with `=` operator.
This means "Select all documents with name = homepage and type = html"

### `add`
    public function add ($field, $operator=null, $value=null, $rfield = null);

`add` method returns new `ObjectSet` of objects from existing with objects added
by criteria. Objects meeting criteria are included to set. This method is
similar to [filter](#filter) except `filter` excludes objects but `add` includes.
Simple usecase is:

    ObjectSet::none ('Document')->add ('name', '=', 'homepage');

This construction means "Select all documents named 'homepage'". This is equivalent to construction from `filter` sample.
Note that we `add` objects to empty set returned by `none`. There's no reason to add any objects to full set because it
already contains all objects. As well as `filter`, `add` can be chained and
accepts array. Another example:

    ObjectSet::none ('Document')->add ([
        'type' => 'html',
        'type' => 'pdf'
    ]);

This example demonstrates simplified syntax. This is full equivalent of

    ObjectSet::none ('Document')->add ('type', '=', 'pdf')->add ('type', '=', 'html');

This means "Select all Documents with type = pdf OR type = html". We create
empty ObjectSet and then add 'type=pdf' objects to it, and then 'type=html'
objects.

### `slice`
    public function ObjectSet::slice ($offset, $count);

`slice` method returns part of existing `ObjectSet` starting from `$offset` of
`$count` length at most. In current implementation this function adds LIMIT
and OFFSET to SQL queries. Slices can be chained as well as any other ObjectSet
methods.

    ObjectSet:: all('Document')->slice (0, 3)->slice (2, 3);

This example shows ObjectSet returning single Document and 2nd position
(counting from 0) of set. This is how it is achieved:

    ObjectSet       Elements
    ObjectSet::all  0 1 2 3 4 5 6 7 8 9 ...
    ->slice (0, 3)  0 1 2
    ->slice (2, 3)      2 X X

`X X` elements aren't returned as they are in the second object set.

### `sortby`
    public function ObjectSet::sortby ($field, $reverse=false);

`sortby` method orders objects in set by `$field`. Currently this method results
in `ORDER BY` clause in SQL query. Set `$reverse` parameter to `True` to order
objects in reverse order.

!!! `sortby` cannot be chained! Each call to `sortby` method overrides previous
call. This behaviour can and will be changed in future versions of Futuorm.

