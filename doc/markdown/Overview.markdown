Overview
========
Futuorm is an [Object-relational mapping](http://en.wikipedia.org/wiki/Object-relational_mapping) toolkit for PHP.
Its primary purpose is to keep mapping between PHP classes and SQL tables, track relations between classes/tables and
automatically build SQL queries based on PHP code.

Project goals
-------------
Project is aimed at usage simplicity and efficiency.

Simplicity means that any developer using Futuorm doesn't need to write the
*how* code, he writes *what* code instead. Complex SQL queries are hidden under
simple PHP code. I hate writing SQL code, and I want my computer will do it for
me.

Efficiency means that SQL queries emitted by Futuorm are supposed to be as fast
as possible.

Note that Futuorm cannot *think* instead of you. You still have to know what are
you doing while writing code.

Usage
-----
To use Futuorm in your PHP code you should first include futuorm.php into your script like this:

    require_once 'futuorm.php';

Then you should point Futuorm to your specific database configuration (see [Database Setup](DatabaseSetup)):

    require_once 'mysql-backend.php';
    $backend = new Mysql_backend ('test_database', 'localhost', 'root', 'rootpwd');
    ObjectSet::init ($backend);
    
Now you can define models (see [Models](Models)):
    
    class User extends Model
    {
        public $username = array ('Text', 'length' => 64, 'index', 'unique');
        public $registered = array ('DateTime', 'creation');
    }

    class Post extends Model
    {
        public $title = array ('Text', 'length' => 128);
        public $body = array ('Text');
        public $author = array ('Reference', 'to' => 'User');
        public $tags = array ('Set', 'of' => 'Tag');
    }

    class Tag extends Model
    {
        public $slug = array ('Text', 'length' => 64, 'unique', 'index');
    }

and create database schema (see [Table Creation](DatabaseSetup#Table_creation)):

    ObjectSet::setup();

You can create new objects (see [Creating Instances](Models#Creating_Instances)):

    $user = new User (array ('username' => 'john'));
    $user->save();

    $post = new Post (array (
        'title' => 'First blog post',
        'body' => 'blah-blah',
        'author' => $user
        ));
    $post->save();

pick them from database (see [Object Sets](ObjectSets)):

    $john = ObjectSet::all ('John')->filter (array (
        'username' => 'john',
        'active' => true
    ));

and even find referenced objects (see [Relations](Relations)):

    $posts = $john->Post_set;

