Database setup
==============

Backends
--------

Currently Futuorm has three SQL backends: [Mysql_backend](#Mysql_backend), [Sqlite_backend](#Sqlite_backend)
and [Postgres_backend](#Postgres_backend). Backends are usual PHP classes that can be
instantiated using constructors. Futuorm can be instructed to use particular
backend by `ObjectSet::init()` static method. See following example.

### `Mysql_backend`
Declared in `mysql-backend.php`

### `Sqlite_backend`
Declared in `sqlite-backend.php`

Table creation
--------------
Futuorm can create SQL tables automatically. It's done by `ObjectSet::setup()`
static method. This method creates all SQL tables needed to maintain currently
defined `Model`s.
Note that this method requires properly created and configured
backend. See following example

Example
-------
    require_once 'futuorm.php'; //Include Futuorm core file
    require_once 'mysql-backend.php'; //Include neede Futuorm SQL backend

    //Defining models:
    class Model1 extends Model
    {
        public $val1 = ['Integer'];
        public $set1 = ['Set', 'of' => 'Model2'];
    }

    class Model2 extends Model
    {
        public $val2 = ['Text'];
        public $ref2 = ['Reference', 'to' => 'Model1'];
    }

    //Creating Mysql backend:
    //All arguments except first are optional
    $backend = new Mysql_backend ('mydb', 'localhost', 3306, 'sqluser', 'sqlpwd');
    ObjectSet::init ($backend); //Instruct Futuorm to use our backend
    ObjectSet::setup (); //Create tables

### Database migration
!!! `ObjectSet::setup()` will *not* do database migration for you. It means that
if you have already created your tables, `setup()` will *not* create missing
fields or remove spare ones. `setup()` will emit `CREATE TABLE` queries that
will conflict your existing schema. Database migration is currently out of
Futuorm scope.
