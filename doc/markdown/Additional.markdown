Additional
=============

Optimization
------------

As project goals say, Futuorm tries (and I hope succeeds at) to emit as much efficient SQL queries as
possible.

Objects aren't retrieved from database unless they are really needed. Let's use
models from previous chapters:

    class User extends Model
    {
        public $name = ['Text'];
        public $email = ['Text'];
    }

    class Post extends Model
    {
        public $text = ['Text'];
        public $author = ['Reference', 'to' => 'User'];
    }

`$author` is lazy reference. Lazyness means that

    $post = ObjectSet::byId ('Post', 1);

will pick Post from database, and not User referenced by it. And then

    $username = $post->author->name;

will emit another SQL query to pick post's author from database. This approach
is efficient when author name is rarely needed. But in case when author name is
needed on every or almost every Post access, these two queries can be merged to
single one

    class User extends Model
    {
        public $name = ['Text'];
        public $email = ['Text'];
    }

    class Post extends Model
    {
        public $text = ['Text'];
        public $author = ['Reference', 'to' => 'User', 'prefetch'];
    }

Note `prefetch` attribute appeared at `$author` declaration. This attribute will
make reference non-lazy, so Post and its author will be fetched in a single
query (using SQL JOIN).

Futuorm will have more optimization techniques later.

Debugging
---------

Input checks vs Speed
---------------------
Speed. Futuorm won't check that you create UNIQUE constraint or INDEX on TEXT field. It won't check unqiue or
foreign key constraints for you because DB can do it. You will know it from PDOExceptions.
