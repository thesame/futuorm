<?php

function
h1 ($s)
{
    return "= $s =\n";
}

function
h2 ($s)
{
    return "== $s ==\n";
}

$src = file_get_contents ($argv[1]);

$pars = preg_split ("/\n\n+/", $src);

foreach ($pars as &$s)
{
    $lines = explode ("\n", $s);
    if ((count ($lines) == 2) && preg_match ('/^-+$/', $lines[1]))
    {
        $s = h1 ($lines[0]);
        continue;
    }

    if ((count ($lines) == 2) && preg_match ('/^=+$/', $lines[1]))
    {
        $s = h2 ($lines[0]);
        continue;
    }

    $s = preg_replace ('/^### (.+)$/', '=== \1 ===', $s);
#    $s = preg_replace ('/^(.+)\n-+/m', '= \1 =', $s);
#    $s = preg_replace ('/^(.+)\n=+/m', '== \1 ==', $s);
}
echo implode ("\n", $pars);
#echo $s;
