Models
======

Defining Models
--------

Basic concept of ORM is Model. Model is a PHP class representing SQL table (and
vice versa). Defining a model (and SQL table representing it) is as simple as
defining a PHP class. Each field should be public property with initial value of
array defining field type and attributes:

    class MyFutuormClass extends Model
    {
        public $intval = ['Integer', 'default' => 42, 'unique']; //Field `intval` of `Integer` type
        public $strval = ['Text', 'length' => 64]; //Field `strval` of `Text` type
    }

You can see two fields declared, `intval` of type `Integer` and `strval` of type
`Text`. `intval` has `default` and `unique` attributes also. As you can see, attributes can be
of both `'key'=>'value'` and `'value'` forms.

This class will be represented by SQL table like this:

    mysql> describe MyFutuormClass;
    +--------+-------------+------+-----+---------+----------------+
    | Field  | Type        | Null | Key | Default | Extra          |
    +--------+-------------+------+-----+---------+----------------+
    | id     | int(11)     | NO   | PRI | NULL    | auto_increment |
    | intval | int(11)     | YES  | UNI | NULL    |                |
    | strval | varchar(64) | YES  |     | NULL    |                |
    +--------+-------------+------+-----+---------+----------------+


Creating Instances
------------------

Once Model is defined, one can create its instance and store it in database. Instance is created
as usual PHP objects by its constructor and is saved by `save()` method.
Pretty obvious functions:

    $myobject = new MyFutuormClass ([
        'intval' => 42,
        'strval' => 'Hello'
    ]);
    $myobject->save();

Note that object instance is created simply by constructor that accepts optional
array of initial values. Method `save()` saves instance to database. Saving can
imply INSERT or UPDATE query depending on previous current state of object.

Modifiying Instances
--------------------

Modifiying of existing object is also done by `save()` method:

    $myobject->intval = 43;
    $myobject->save();

There is another way to modify object properties. This method allows to modify
multiple properties at once:

    $myobject->update ([
        'intval' => 44,
        'strval' => 'Bye'
    ]);

This snippet will update both `intval` and `strval`.

`update` method also accepts `$allowed` parameter that restricts it
on updating only predefined set of values. For example:

    $myobject->update ([
        'intval' => 45, //this will be updated
        'strval' => 'This will be ignored',
        'accidentally' => array ('Some kind of value')
        ],
        ['intval'] //This is $allowed
    );

Note that this snipped only updates `intval` property of object, ignoring `'strval'` and `'accidentally'`.
This can be useful when you have, for example `$_REQUEST` array filled with bunch of fields from
form, uri and cookie. You can do:

    $myobject->update ($_REQUEST, ['first_name', 'last_name']);

In this case `update` will update only `first_name` and `last_name` properties
ignoring any other array keys. Very useful option.

Deleting Instances
------------------

Any object or set of objects can be deleted by `delete()` method.

### `delete`
`delete()` method deletes object from database. Any integrity checks are
performed by database engine (simulated integrity checks are enqueued in TODO
list). Futuorm will delete object being deleted from all [many-to-many](Relations#Many-to-many) sets
automatically.

    $myobject->delete();

Types
-----

Futuorm supports sane set of data types for Models properties. These types include:

### Integer
Properties of `'Integer'` type store integer values. Currently represented by `INTEGER` SQL value. Accepts `'unsigned'`
optional attribute.

#### `unsigned`
Makes unsigned Integer. (`UNSIGNED INTEGER` SQL type).

### Boolean
Properties of `'Boolean'` type can take only `true` and `false` values.

### DateTime
Stores date and time in form of unix timestamp. `DateTime` type
accepts optional attributes `'creation'` and `'modification'`.

#### `creation`
`'creation'` is optional attribute that makes `'DateTime'` property automatically
take value of object creation time. Stays unmodified after updates.

    class User extends Model
    {
        ...
        public $date_registered = ['DateTime', 'creation'];
        ...
    }

#### `modification`
`'modification'` is optional attribute that makes `'DateTime'` property automatically accept value of current timestamp
at the moment of saving.

    class Document extends Model
    {
        ...
        public $last_modified = ['DateTime', 'modification'];
        ...
    }

### Choice
Choice accepts a value from predefined set of values. It accepts mandatory attribute `'choices'` defining set
of values.

    class User extends Model
    {
        ...
        public $gender = ['Choice', 'choices' => ['male', 'female']];
        ...
    }

#### `choices`
Mandatory attribute of `Choices` type definigf set of possible values.

### Float
`'Float'` is scalar float value. Currently represented by `FLOAT` SQL type.

### Text
Properties of `'Text'` type store text values. In current implementation of Futuorm database representation varies on
type properties:

#### `length`
`'length'` attribute when specified makes value to be stored in `VARCHAR` field. When absent, makes `TEXT`.
Model:

    class BlogPost extends Model
    {
        public $slug = ['Text', 'length' => 128];
        public $body = ['Text'];
    }

will produce:

    mysql> describe BlogPost;
    +-------+--------------+------+-----+---------+----------------+
    | Field | Type         | Null | Key | Default | Extra          |
    +-------+--------------+------+-----+---------+----------------+
    | id    | int(11)      | NO   | PRI | NULL    | auto_increment |
    | slug  | varchar(128) | YES  |     | NULL    |                |
    | body  | text         | YES  |     | NULL    |                |
    +-------+--------------+------+-----+---------+----------------+


### Raw
`'Raw'` type of property allows to store any (serializable using [Serialize](http://php.net/serialize)) PHP value.
Values are stored serialized to `TEXT` field.

### Reference
Stores reference to another model. See [Referenes](Relations#References).

### Set
Stores set of another model instances. See [many-to-many](Relations#Many-to-many).

### Additional type attributes

#### `index`
Makes Futuorm to create index on specified field. Knowledge of indeces is basic knowledge about SQL and is out of
scope of this document.

#### `unique`
Creates `UNIQUE` constraint on database field.

#### `required`
Creates `NOT NULL` constraint on database field.
