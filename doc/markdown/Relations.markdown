Relations
=========

ORM would be useless if it couldn't keep relations between objects. See
[Cardinality](http://en.wikipedia.org/wiki/Cardinality_(data_modeling)) for detailed
explaination of table relations within database design. Futuorm supports two
types of relations, one-to-many ([referenes](#References)) and [many-to-many](#Many-to-many).

References
----------

`Reference` implements one-to-many relation between two models. Consider an
example where blog database contains records about blog posts and users posted
these posts:

    class User extends Model
    {
        public $name = ['Text'];
        public $email = ['Text'];
    }

    class Post extends Model
    {
        public $text = ['Text'];
        public $author = ['Reference', 'to' => 'User'];
    }

Any object of `Post` class will refer single object of `User` using `author`
field. This reference allows

1. to find out author of post
2. to find all posts of author (see [Backward references](#Backward_references))
3. to make sure author having posts is not deleted (see
[Integrity constraints](http://en.wikipedia.org/wiki/Integrity_constraints))

Although references are "lazy" (unless [prefetched](#prefetch)), accessing referenced
objects is fully automated. You can access referenced object as any other field:

    //assuming $post is object of Post class
    $username = $post->author->name;

'Laziness' of references mean that referenced object will be fetched once it's
needed.

### `prefetch`
`prefetch` is a special attribute that makes referenced object be fetched in a
single query with parent object. It can be used in optimisation purpose. See
[Optimization](Additional#Optimization).

Backward references
-------------------

`Reference` fields allow object to 'know' set of objects referencing him by
pseudo-attribute `<Classname>_set`. We can find all posts posted by particular
`$user`:

    //assuming $user is object of User class
    foreach ($user->Post_set as $post)
        output ($post);

Pseudo-attribute `Post_set` is constructed of class name `Post` and `_set`
suffix. Note that if `Post` had two or more `Reference`s to `User`, `Post_set`
would return set referenced by first one. See [`get_set_by_field`](#get_set_by_field) for
details.

### `get_set_by_field`

    public function Model::get_set_by_field ($class, $field);

`get_set_by_field` method of model can be useful when model has more than
one reference another model, and both backward references are needed. Consider
an example:

    class User extends Model
    {
        public $name = ['Text'];
        public $email = ['Text'];
    }

    class Post extends Model
    {
        public $text = ['Text'];
        public $author = ['Reference', 'to' => 'User'];
        public $corrector = ['Reference', 'to' => 'User'];
    }

This is slightly modified code of previous example. You can see that `Post`
model has two fields `Reference`ing `User` model. These fields are called
`author` and `corrector`.

If we need to know all posts authored by `$user` we still can do:

    $posts = $user->Post_set;

`Post_set` attribute will return `ObjectSet` of `Post` objects whose `$author`
field points to `$user`. This happens because `$author` is the first field of
these two.

If we need to know all posts corrected by `$user` we can do:

    $posts = $user->get_set_by_field ('Post', 'corrector');

Many-to-many
------------

Many-to-many relationship between models is made by `Set` fields. `Set` is
similar to `Reference` in some ways, but it can reference not a single object,
but set of objects. We suppose our blog application has tags attached to posts
(think of many-to-many as "tag can be attached to many posts; post can attach
many tags"). Our models could look like this:

    class Post extends Model
    {
        public $text = ['Text'];
        public $tags = ['Set', 'of' => 'Tag'];
    }

    class Tag extends Model
    {
        public $slug = ['Text', 'length' => 64];
    }

Futuorm will automatically keep tracking of references between tags and posts
(this will require additional table though).

### Referencing

#### Forward
Having object of `Post` class we can fetch all tags attached to it:

    //assuming $post is object of Post class
    $tags = $post->tags_set;
    foreach ($tags as $tag)
        output ($tag); //hypothetical function

Note that set of object is accessed by field name `tags` and `_set` suffix.

#### Backward
`Set` fields support backward references as well as `Reference` field do. One
can pick all `Post`s having `$tag` attached by the following code:

    //assuming $tag is object of Tag
    $posts = $tag->Post_set;

### Adding objects to set
Special pseudo-methods are used to add objects to many-to-many references. These
methods are constructed of field name and `_add` suffix. For example:

    //assuming $post is object of Post class
    $tag = new Tag (['slug' => 'futuorm']); //create new Tag with slug = 'futuorm'
    $tag->save(); //save it to DB
    $post->tags_add ($tag); //attach it to $post object

This pseudo-method will instantly add object to set. There's no need to call
`$post->save()` after it.

### Deleting objects from set
Special pseudo-methods are used to delete objects from many-to-many references. These
methods are constructed of field name and `_del` suffix. For example:

    //assuming $post is object of Post class
    $tag = ObjectSet::byId ('Tag', 1);
    $post->tags_del ($tag);

This pseudo-method will instantly delete object from set. There's no need to call
`$post->save()` after it.
