 * [Overview#Overview Overview]
  * [Overview#Project_goals Project goals]
  * [Overview#Usage Usage]
 * [Models#Models Models]
  * [Models#Defining_Models Defining Models]
  * [Models#Creating_Instances Creating Instances]
  * [Models#Modifiying_Instances Modifiying Instances]
  * [Models#Deleting_Instances Deleting Instances]
   * [Models#delete delete]
  * [Models#Types Types]
   * [Models#Integer Integer]
    * [Models#unsigned unsigned]
   * [Models#Boolean Boolean]
   * [Models#DateTime DateTime]
    * [Models#creation creation]
    * [Models#modification modification]
   * [Models#Choice Choice]
    * [Models#choices choices]
   * [Models#Float Float]
   * [Models#Text Text]
    * [Models#length length]
   * [Models#Raw Raw]
   * [Models#Reference Reference]
   * [Models#Set Set]
   * [Models#Additional_type_attributes Additional type attributes]
    * [Models#index index]
    * [Models#unique unique]
    * [Models#required required]
 * [ObjectSets#Object_sets Object sets]
  * [ObjectSets#Creating Creating]
   * [ObjectSets#all all]
   * [ObjectSets#none none]
   * [ObjectSets#byId byId]
  * [ObjectSets#Fetching_objects Fetching objects]
   * [ObjectSets#iterating iterating]
   * [ObjectSets#one one]
  * [ObjectSets#Modifiying Modifiying]
   * [ObjectSets#filter filter]
   * [ObjectSets#add add]
   * [ObjectSets#slice slice]
   * [ObjectSets#sortby sortby]
 * [Relations#Relations Relations]
  * [Relations#References References]
   * [Relations#prefetch prefetch]
  * [Relations#Backward_references Backward references]
   * [Relations#get_set_by_field get_set_by_field]
  * [Relations#Many-to-many Many-to-many]
   * [Relations#Referencing Referencing]
    * [Relations#Forward Forward]
    * [Relations#Backward Backward]
   * [Relations#Adding_objects_to_set Adding objects to set]
   * [Relations#Deleting_objects_from_set Deleting objects from set]
 * [DatabaseSetup#Database_setup Database setup]
  * [DatabaseSetup#Backends Backends]
   * [DatabaseSetup#Mysql_backend Mysql_backend]
   * [DatabaseSetup#Sqlite_backend Sqlite_backend]
  * [DatabaseSetup#Table_creation Table creation]
  * [DatabaseSetup#Example Example]
   * [DatabaseSetup#Database_migration Database migration]
 * [Additional#Additional Additional]
  * [Additional#Optimization Optimization]
  * [Additional#Debugging Debugging]
  * [Additional#Input_checks_vs_Speed Input checks vs Speed]
