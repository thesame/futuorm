<?php

require_once 'common-backend.php';

class Mysql_backend extends Common_backend
{
    public $pdo;
    protected $dsn;
    protected $username;
    protected $pwd;

    public static $error = array (
        'duplicate' => 23000
    );

    public function
    __construct ($dbname, $host = null, $port = null, $username = null, $pwd = null)
    {
        $this->dsn = "mysql:dbname=$dbname";
        if ($host) $this->dsn .= ";host=$host";
        if ($port) $this->dsn .= ";port=$port";
        $this->username = $username;
        $this->pwd = $pwd;
    }

    public function
    getPDO ()
    {
        $this->pdo = new PDO ($this->dsn, $this->username, $this->pwd);
        $this->pdo->query ('SET sql_mode=\'ANSI_QUOTES\'');
        $this->pdo->query ('SET NAMES \'utf8\'');
        $this->pdo->setAttribute (PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $this->pdo;
    }

    public function
    get_sql_type ($class, $field, $ann)
    {
        if ($ann[0] == 'Choice')
            return 'ENUM (\''.implode ('\', \'', $ann['choices']).'\')';

        return parent::get_sql_type ($class, $field, $ann);
    }

    public function
    last_insert_id ($class)
    {
        return $this->pdo->lastInsertId();
    }

    protected function
    get_primary_type ()
    {
        return 'INTEGER PRIMARY KEY AUTO_INCREMENT';
    }

    protected function
    get_table_postfix ()
    {
        return "ENGINE=InnoDB CHARACTER SET 'utf8'";
    }
}

