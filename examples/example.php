<?php

/**
 *
 */

die('This code is not intended to be run. It\'s for demo purposes only');

/**
 * This file contains examples of Futuorm usecases. It consists of few parts
 * divided by noticeable comments. In this example we assume we're creating
 * some parts of code of weblog application. Blog contains entries. Every
 * entry is posted by user and is tagged by some tags. Also we have comments.
 * So, here we go:
 */

// Including Futuorm library
require_once ('futuorm.php');

/**
 * Part 1. Defning models.
 * Every model is represented by SQL table. Every row in
 * such a table is represented by PHP object instance of model class.
 * Every model is inherited from Model class.
 */

/**
 * Model representing entry author
 */
class Author extends Model
{
    /**
     * Here we claim Author has name with maximum length of 64 characters.
     * 'length' parameter is optional.
     * 'index' makes field to be indexed in database.
     */
    public $name = array ('Text', 'length'=>64, 'index');

    /**
     * DateTime stores unix timestamp in db.
     * 'creation' makes field to contain timestamp of object creation
     * 'modification' makes field to contain timestamp
     *      of last object modification
     */
    public $joined = array ('DateTime', 'creation');
}

class Tag extends Model
{
    /**
     * 'required' means field cannot be NULL.
     *  Note empty string is still allowed.
     */
    public $name = array ('Text', 'length'=>64, 'index', 'required');
}

class Entry extends Model
{
    public $title = array ('Text', 'length'=>256);

    /**
     * No maximum length here. Field stores as much text as database allows
     */
    public $body = array ('Text');

    /**
     * Futuorm can automatically keep references between objects.
     * This definition means $author will keep reference to Author
     * 'required' parameter disallows this field to be NULL
     */
    public $author = array ('Reference', 'to'=>'Author', 'required');

    /**
     * This definition means many-to-many relation between Entry and Tag.
     * So, each Entry can refer to multiple Tag, and each Tag can refer
     * to multiple relation.
     */   
    public $tags = array ('Set', 'of'=>'Tag');
    
    public $posted = array ('DateTime', 'creation', 'index');
    public $modified = array ('DateTime', 'modification');
}

die('This code is not intended to be run. It\'s for demo purposes only');

/**
 * Part 2. Creating schema.
 * Now we have all models defined. Creating SQL schema:
 */
 
require_once ('setup.php');
 
/**
 * This function will output all needed sql queries to screen. Execute them
 * on your SQL server in any accessible way.
 */
setup();
 
die('This code is not intended to be run. It\'s for demo purposes only');

/**
 * Part 3. Creating objects.
 * This part describes ways to create objects.
 */

$author1 = new Author (array (
    'name' => 'Anny'
));
// Now $author1 contains instance of John Doe. It's now in database yet, but it
// will be now...

/**
 * Saves object $author1 to database. Note that this call won't save referenced
 * objects. Use ->save_all() to save referenced objects.
 */
$author1->save();

$author2 = new Author();
$author2->name = 'Ben';

$entry = new Entry (array (
    'title' => 'First blog entry',
    'body' => 'This is our first entry',
    'author' => $author2
));

/**
 * Saving $entry to database. Note that referenced objects are also saved so
 * $author2 gets to database too.
 */
$entry->save_all();

die('This code is not intended to be run. It\'s for demo purposes only');

/**
 * Part 4. Selecting objects from database.
 * We have class ObjectSet to make queries to
 * database. For example:
 */

/**
 * This call returns full set of entries. It means $all_entries will contain all
 * entries stored in database.
 */
$all_entries = ObjectSet:all ('Entry');

/**
 * This call returns empty set of entries. IT meanse $no_entries will contain no
 * objects at all.
 */
$no_entries = ObjectSet:none ('Entry');

/**
 * We also have a short cirtuit to fetch particular object by ID.
 */
$entry1 = ObjectSet::byId ('Entry', 1);

/**
 * This construction allows to fetch all entries posted in future.
 * Explanation: we take full set of entries and filter it by condition. We also
 * have a couple ways to do it.
 */
$future_entries = ObjectSet::all ('Entry')
    ->filter ('posted', '>', time());

/**
 * This construction is synonymous to previous one.
 * Explanation: we take empty set of entries and add needed entries to it.
 */
$future_entries2 = ObjectSet::none ('Entry')
    ->add ('posted', '>', time());

/**
 * ->filter() and ->add() methods also accept arrays. A pretty useless
 * query follows
 */
$some_entries = ObjectSet::all ('Entry')
    ->filter (array (
        'posted' => time(),
        'author' => ObjectSet::byId ('Author', 1)
    ));

die('This code is not intended to be run. It\'s for demo purposes only');

/**
 * Part 5. Iteration and counting.
 * ObjectSet's can be iterated and counted
 */

$entries = ObjectSet::all ('Entry');

if (count ($entries) == 0)
    echo "We've got no entries";
else
{
    foreach ($entries as $entry)
        echo $entry->title."\n";
}

die('This code is not intended to be run. It\'s for demo purposes only');

/**
 * Part 6. Object deletion.
 * Objects can be deleted in two ways:
 */

/**
 * First, by instance:
 */
$author = ObjectSet::byId ('Author', 1);

/**
 * Delete exactly this author
 */
$author->delete();

/**
 * Second, by object set:
 */
$authors = ObjectSet::all ('Author')
    ->filter ('joined', '>', time()-68400);
/**
 * Delete all authors belonging to this object set
 */
$authors->delete();

die('This code is not intended to be run. It\'s for demo purposes only');

/**
 * Part 7. Many-to-many relations.
 * Futuorm can keep many-to-many references automatically. It has special
 * methods for it:
 */

$entry = ObjectSet ('Entry', 1);
$tag = ObjectSet:all ('Tag')->filter (array ('name' => 'examples'));

/**
 * Link this $entry and this $tag. We call "tags_add" because we have "tags"
 * annotation in Entry. This call saves link but doesn't save objects itselves
 * so you have to save them manually before linking.
 */
$entry->tags_add ($tag);

/**
 * Unlink $entry and $tag
 */
$entry->tags_remove ($tag);

/**
 * Check whether $entry and $tag are linked
 */
echo $entry->tags_has ($tag) ? 'Does' : 'Doesn\'t';

die('This code is not intended to be run. It\'s for demo purposes only');

/**
 * Part 8. Reverse sets.
 * Futuorm can handle reverse sets for you.
 */

$author = ObjectSet::byId ('Author', 1);

/**
 * This construction will return set of all entries posted by $author.
 * If Entry model contained two or more references to Author (for example,
 * "public $reviewer = array ('Reference', 'to'=>'Author')" in addition to
 * "public $author" field), the first reference would be used.
 */
$posts = $author->Entry_set;

/**
 * Reverse sets are working for many-to-many relations too.
 */
$tag = ObjectSet::byId ('Tag', 1);

/**
 * Although Tag model haven't implicit reference to Entry, reverse is still
 * working:
 */
$entries = $tag->Entry_set;

/**
 *
 */
