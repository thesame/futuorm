<?php

// Model representing entry author
class Author extends Model
{
    // Here we claim Author has name with maximum length of 64 characters.
    // length' parameter is optional.
    // 'index' makes field to be indexed in database.
    public $name = array ('Text', 'length'=>64, 'index');

    // DateTime stores unix timestamp in db.
    // 'creation' makes field to contain timestamp of object creation
    // 'modification' makes field to contain timestamp
    //     of last object modification
    public $joined = array ('DateTime', 'creation');
}

class Tag extends Model
{
    /**
     * 'required' means field cannot be NULL.
     *  Note empty string is still allowed.
     */
    public $name = array ('Text', 'length'=>64, 'index', 'required');
}

class Entry extends Model
{
    public $title = array ('Text', 'length'=>256);

    /**
     * No maximum length here. Field stores as much text as database allows
     */
    public $body = array ('Text');

    /**
     * Futuorm can automatically keep references between objects.
     * This definition means $author will keep reference to Author
     * 'required' parameter disallows this field to be NULL
     */
    public $author = array ('Reference', 'to'=>'Author', 'required');

    /**
     * This definition means many-to-many relation between Entry and Tag.
     * So, each Entry can refer to multiple Tag, and each Tag can refer
     * to multiple relation.
     */   
    public $tags = array ('Set', 'of'=>'Tag');
    
    public $posted = array ('DateTime', 'creation', 'index');
    public $modified = array ('DateTime', 'modification');

