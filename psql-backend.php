<?php

class Postgres_backend extends Common_backend
{
    public $pdo;
    protected $dbname;

    public static $error = array (
        'duplicate' => 23505
    );

    public function
    __construct ($dbname)
    {
        $this->dbname = $dbname;
    }

    public function
    getPDO ()
    {
        $this->pdo = new PDO ("pgsql:dbname={$this->dbname}", 'postgres');
        $this->pdo->setAttribute (PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $this->pdo;
    }

    public function
    get_sql_type ($class, $field, $annotation)
    {
        if ($annotation[0] == 'Choice')
            return "\"{$class}_{$field}_enum\"";

        return parent::get_sql_type ($class, $field, $annotation);
    }

    public function
    last_insert_id ($class)
    {
        return $this->pdo->lastInsertId ("\"{$class}_id_seq\"");
    }

    protected function
    get_primary_type ()
    {
        return 'SERIAL PRIMARY KEY';
    }

    protected function
    setup_pre_schema ($class)
    {
        $res = array();
        foreach (Base::find_anns_of ($class) as $field => $ann)
        {
            if ($ann[0] == 'Choice')
                $res[] = 'CREATE TYPE "'.$class.'_'.$field.'_enum" AS ENUM(\''.implode ('\',\'', $ann['choices']).'\')';
        }
        return $res;
    }
}
