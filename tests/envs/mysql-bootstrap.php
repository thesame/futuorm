<?php

define ('DEBUG', true);
require_once dirname (__FILE__).'/../../futuorm.php';
require_once dirname (__FILE__).'/../../mysql-backend.php';

abstract class FutuormCommonTest extends PHPUnit_Framework_TestCase
{
    public function
    prepare_db ()
    {
        $pdo = new PDO ('mysql:');
        $pdo->setAttribute (PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        if ($pdo->query ('SELECT COUNT(SCHEMA_NAME) FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = \'test\'')->fetchColumn() > 0)
            die ("Mysql database \"test\" already exists; I won't overwrite it\n");
        $pdo->exec ('CREATE DATABASE test');
        $pdo = null;

        $backend = new Mysql_backend ('test');
        ObjectSet::init ($backend);
        ObjectSet::setup();
    }

    public function
    tearDown ()
    {
        $pdo = new PDO('mysql:');
        $pdo->exec ('DROP DATABASE test');
        $pdo = null;
    }
}

