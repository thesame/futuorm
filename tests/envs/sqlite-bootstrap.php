<?php

define ('DEBUG', true);
require_once dirname(__FILE__).'/../../futuorm.php';
require_once dirname(__FILE__).'/../../sqlite-backend.php';

abstract class FutuormCommonTest extends PHPUnit_Framework_TestCase
{
    public function
    prepare_db ()
    {
        if (file_exists ('test.sqlite'))
            die ("test.sqlite already exists; I won't overwrite it\n");

        $backend = new Sqlite_backend ('test.sqlite');
        ObjectSet::init ($backend);
        ObjectSet::setup();
    }

    function
    tearDown ()
    {
        unlink ('test.sqlite');
    }
}

