<?php

class LimitTestModel extends Model
{
    public $val = ['Integer'];
}

class LimitsTest extends FutuormCommonTest
{
    public function
    setUp ()
    {
        $this->prepare_db();

        for ($i = 0; $i < 12; ++$i)
        {
            $c = new LimitTestModel (['val' => $i]);
            $c->save();
        }
    }

    public function
    testLimit ()
    {
        $this->assertCount (
            5,
            ObjectSet::all ('LimitTestModel')->slice (0, 5)
        );
    }

    public function
    testDoubleLimit ()
    {
        $this->assertCount (
            3,
            ObjectSet::all ('LimitTestModel')->slice (0, 5)->slice (2, 1000)
        );
    }

    public function
    testDeleteLimit ()
    {
        ObjectSet::all ('LimitTestModel')->slice(0,5)->delete();

        $this->assertCount (
            7,
            ObjectSet::all ('LimitTestModel')
        );
    }
}

