<?php

class Referer extends Model
{
    public $ref = ['Reference', 'to' => 'Referee'];
    public $val = ['Integer'];
}

class Referee extends Model
{
    public $val = ['Integer'];
}

class TestReferences extends FutuormCommonTest
{
    protected function
    setUp ()
    {
        $this->prepare_db();

        $r1 = new Referee (['val' => 42]);
        $r1->save();

        $r2 = new Referer (['val' => 43, 'ref' => $r1]);
        $r2->save();

        $r22 = new Referer (['val' => 44, 'ref' => $r1]);
        $r22->save();
    }

    public function
    testReference ()
    {
        $this->assertInstanceOf (
            'Referee',
            ObjectSet::all ('Referer')->one->ref
        );

        $this->assertEquals (
            42,
            ObjectSet::all ('Referer')->one->ref->val
        );
    }

    public function
    testBackreference ()
    {
        $this->assertInstanceOf (
            'ObjectSet',
            ObjectSet::all ('Referee')->one->Referer_set
        );

        $this->assertCount (
            2,
            ObjectSet::all ('Referee')->one->Referer_set
        );

        $this->assertInstanceOf (
            'Referer',
            ObjectSet::all ('Referee')->one->Referer_set->one
        );

        $this->assertEquals (
            43,
            ObjectSet::all ('Referee')->one->Referer_set->one->val
        );
    }
    
    /**
     * @expectedException PDOException
     */
    public function
    testIntegrityExceptionCheck ()
    {
        ObjectSet::all('Referee')->one->delete();
    }

    public function
    testIntegritySuccessCheck ()
    {
        ObjectSet::all('Referer')->delete();
        ObjectSet::all('Referee')->one->delete();
    }
}

