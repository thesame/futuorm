<?php

/*
 * Tests covering Many-to-Many relationships in Futuorm.
 *
 * Note that althouth tests are working, they are not completely correct
 * because they imply that records in DB are stored in the order they added
 * and IDs are given starting from 1 and so forth.
 */

class M2MC1 extends Model
{
    public $val = ['Integer'];
}

class M2MC2 extends Model
{
    public $val = ['Integer'];
    public $c1s = ['Set', 'of' => 'M2MC1'];
    public $c1s2 = ['Set', 'of' => 'M2MC1'];
}

class TestM2M extends FutuormCommonTest
{
    protected function
    setUp ()
    {
        $this->prepare_db();

        $c1 = new M2MC1 (['val' => 42]);
        $c1->save();

        $c2 = new M2MC2 (['val' => 43]);
        $c2->save();

        $c12 = new M2MC1 (['val' => 44]);
        $c12->save();

        $c2->c1s_add ($c1);
        $c2->c1s2_add ($c12);
    }

    /*
     * Direct referencing by field name
     */
    public function
    testRef1 ()
    {
        $this->assertInstanceOf (
            'ObjectSet',
            ObjectSet::all ('M2MC2')->one->c1s_set
        );

        $this->assertCount (
            1,
            ObjectSet::all ('M2MC2')->one->c1s_set
        );

        $this->assertInstanceOf (
            'M2MC1',
            ObjectSet::all ('M2MC2')->one->c1s_set->one
        );

        $this->assertEquals (
            ObjectSet::all ('M2MC2')->one->c1s_set->one->val,
            42
        );
    }

    /*
     * Backward referencing by class name
     */
    public function
    testRef2 ()
    {
        $this->assertInstanceOf (
            'ObjectSet',
            ObjectSet::all ('M2MC1')->one->M2MC2_set
        );

        $this->assertCount (
            1,
            ObjectSet::all ('M2MC1')->one->M2MC2_set
        );

        $this->assertInstanceOf (
            'M2MC2',
            ObjectSet::all ('M2MC1')->one->M2MC2_set->one
        );

        $this->assertEquals (
            43,
            ObjectSet::all ('M2MC1')->one->M2MC2_set->one->val
        );
    }

    /*
     * Backward referencing by class and field name
     */
    public function
    testRef3 ()
    {
        $this->assertInstanceOf (
            'ObjectSet',
            ObjectSet::byId ('M2MC1', 2)->get_set_by_field ('M2MC2', 'c1s2')
        );

        $this->assertCount (
            1,
            ObjectSet::byId ('M2MC1', 2)->get_set_by_field ('M2MC2', 'c1s2')
        );

        $this->assertInstanceOf (
            'M2MC2',
            ObjectSet::byId ('M2MC1', 2)->get_set_by_field ('M2MC2', 'c1s2')->one
        );

        $this->assertEquals (
            43,
            ObjectSet::byId ('M2MC1', 2)->get_set_by_field ('M2MC2', 'c1s2')->one->val
        );
    }
    
    public function
    testSetHas ()
    {
        $c1 = ObjectSet::all ('M2MC1')->filter (['val' => 42])->one;
        $c2 = ObjectSet::all ('M2MC2')->filter (['val' => 43])->one;

        $this->assertTrue (
            $c2->c1s_has ($c1)
        );
    }

    public function
    testSetDel ()
    {
        $c1 = ObjectSet::all ('M2MC1')->filter (['val' => 42])->one;
        $c2 = ObjectSet::all ('M2MC2')->filter (['val' => 43])->one;

        $c2->c1s_del ($c1);

        $this->assertFalse (
            $c2->c1s_has ($c1)
        );
    }

    /**
     * @expectedException PHPUnit_Framework_Error
     */
    public function
    testUnknownMethod ()
    {
        $c = new C1();
        $c->donothing();
    }
}

