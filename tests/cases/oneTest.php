<?php

class Types extends Model
{
    public $int = ['Integer'];
    public $varchar = ['Text', 'length' => 64];
    public $text = ['Text'];
    public $bool = ['Boolean'];
    public $float = ['Float'];
    public $choice = ['Choice', 'choices' => ['choice1', 'choice2']];
    public $raw = ['Raw'];
}

class TestTypes extends FutuormCommonTest
{
    protected function
    setUp ()
    {
        $this->prepare_db();

        $t1 = new Types([
            'int' => 42,
            'varchar' => 'hope it works',
            'text' => 'hope it works too',
            'bool' => True,
            'float' => 4.2,
            'choice' => 'choice1',
            'raw' => [3, 'k' => 4, 'tadam']
        ]);
        $t1->save();

        $t2 = new Types([
            'int' => 43,
            'varchar' => 'hope it works as well',
            'text' => 'hope everything works',
            'bool' => False,
            'float' => 4.3,
            'choice' => 'choice2',
            'raw' => [3, 'k' => 4, 'tadam']
        ]);
        $t2->save();
    }

    public function
    testOne ()
    {
        $obj = ObjectSet::all ('Types')->one;

        $this->assertEquals (42, $obj->int);
        $this->assertEquals ('hope it works', $obj->varchar);
        $this->assertEquals ('hope it works too', $obj->text);
        $this->assertEquals (True, $obj->bool);
        $this->assertEquals (4.2, $obj->float);
        $this->assertEquals ('choice1', $obj->choice);
        $this->assertEquals ([3, 'k'=>4, 'tadam'], $obj->raw);
    }

    public function
    testTwo ()
    {
        $set = ObjectSet::all ('Types');
        $set->one;
        $obj = $set->one;

        $this->assertEquals (43, $obj->int);
        $this->assertEquals ('hope it works as well', $obj->varchar);
        $this->assertEquals ('hope everything works', $obj->text);
        $this->assertEquals (False, $obj->bool);
        $this->assertEquals (4.3, $obj->float);
        $this->assertEquals ('choice2', $obj->choice);
        $this->assertEquals ([3, 'k'=>4, 'tadam'], $obj->raw);
    }
}

