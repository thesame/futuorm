<?php

class C1 extends Model
{
    public $val = ['Integer'];
    public $c2s = ['Set', 'of' => 'C2'];
}

class C2 extends Model
{
    public $val = ['Integer'];
    public $ref = ['Reference', 'to' => 'C1'];
}

class C3 extends Model
{
    public $val = ['Integer'];
}

class DeleteText extends FutuormCommonTest
{
    public function
    setUp ()
    {
        $this->prepare_db();

        $c1 = new C1 (['val' => 42]);
        $c1->save();

        $c2 = new C2 (['val' => 43, 'ref' => $c1]);
        $c2->save();

        $c1->c2s_add ($c2);

        $c22 = new C2 (['val' => 44, 'ref' => $c1]);
        $c22->save();

        $c1->c2s_add ($c22);

        $c23 = new C2 (['val' => 45]);
        $c23->save();

        $c3 = new C3 (['val' => 46]);
        $c3->save();
    }

    public function
    testDeleteObject ()
    {
        ObjectSet::byId ('C2', 1)->delete();

        $this->assertCount (
            2,
            ObjectSet::all ('C2')
        );
    }

    public function
    testDeleteSimple ()
    {
        $set = ObjectSet::all ('C2')->filter ('val', '=', 45);
        $set->delete();

        $this->assertCount (
            2,
            ObjectSet::all ('C2')
        );
    }

    public function
    testDeleteFromSet ()
    {
        $set = ObjectSet::all ('C2')->filter ('val', '=', 43);
        $set->delete();

        $this->assertCount (
            2,
            ObjectSet::all ('C2')
        );
    }

    public function
    testDeleteReferenced ()
    {
        $set = ObjectSet::all ('C1')->filter ('val', '=', 42)->c2s_set;
        $set->delete();

        $this->assertCount (
            1,
            ObjectSet::all ('C2')
        );
    }

    public function
    testDeleteM2M ()
    {
        $c1 = ObjectSet::byId ('C1', 1);
        $c2 = ObjectSet::byId ('C2', 1);

        $set = ObjectSet::all ('C1_c2s_C2')->filter (['from' => $c1, 'to' => $c2])->delete();

        $this->assertCount (
            0,
            ObjectSet::all ('C1_c2s_C2')->filter (['from' => $c1, 'to' => $c2])
        );
    }
}

