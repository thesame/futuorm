<?php

class UniqueModel extends Model
{
    public $val = ['Integer', 'unique'];
}

class RequiredModel extends Model
{
    public $required = ['Integer', 'required'];
}

class TestConstraints extends FutuormCommonTest
{
    /**
     * @expectedException PDOException
     */
    public function
    testUnique ()
    {
        $this->prepare_db();

        $c = new UniqueModel (['val' => 42]);
        $c->save();

        $c = new UniqueModel (['val' => 42]);
        $c->save();
    }

    /**
     * @expectedException PDOException
     */
    public function
    testRequired ()
    {
        $this->prepare_db();
        $c = new RequiredModel (['required' => null]);
        $c->save();
    }
}

