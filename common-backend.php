<?php

class Common_backend
{
    protected $deps = array();

    public function
    get_sql_type ($class, $field, $annotation)
    {
        $type = null;
        if ($annotation[0] == 'Text')
        {
            if (isset ($annotation['length']))
                $type = 'VARCHAR ('.$annotation['length'].')';
            else
                $type = 'TEXT';
        }
        if ($annotation[0] == 'Integer')
        {
            $type = 'INTEGER';
            if (in_array ('unsigned', $annotation, true))
                $type .= ' UNSIGNED';
        }

        if ($annotation[0] == 'Reference')
            $type = 'INTEGER';

        if ($annotation[0] == 'DateTime')
            $type = 'INTEGER';

        if ($annotation[0] == 'Boolean')
            $type = 'INTEGER';

        if ($annotation[0] == 'Float')
            $type = 'FLOAT';

        if ($annotation[0] == 'Choice')
        {
            //array_reduce?
            $maxlen = 0;
            foreach ($annotation['choices'] as $choice)
                if ($maxlen < strlen ($choice))
                    $maxlen = strlen ($choice);
            $type = 'VARCHAR('.$maxlen.') CHECK("'.$field.'" IN ('.(in_array ('required', $annotation, true) ? '' : 'NULL, ').'\''.implode('\', \'', $annotation['choices']).'\'))';
        }

        if ($annotation[0] == 'Raw')
            $type = 'TEXT';

        //if (isset ($annotation['default']))
            //$type .= ' DEFAULT '.Helper::sql_value ($annotation, $annotation['default']);

        if (in_array ('required', $annotation, true))
            $type .= ' NOT NULL';

        if ($type === null)
            return null;

        return $type;
    }

    public function
    sql_value ($annotation, &$value, $rfield = null, $creation = false)
    {
        //FIXME: ann or annotation?
        //if ($annotation[0] == 'Choice')
        //    return array_search ($value, $annotation['choices']);
        return '\''.addslashes ((string)$value).'\'';
    }

    protected function
    setup_pre_schema ($class)
    {
        return array();
    }

    protected function
    table_extra ($class)
    {
        $res = '';
        $anns = get_class_vars ($class);
        foreach ($anns as $field => $ann)
        {
            if ($ann[0] != 'Reference')
                continue;
            $res .= "\tFOREIGN KEY (\"$field\") REFERENCES \"{$ann['to']}\"(\"id\"),\n";
        }
        return $res;
    }
    
    protected function
    setup_schema ($class)
    {
        $res = array();
        $q = "CREATE TABLE \"$class\" (\n";
        if (!is_subclass_of ($class, 'BaseM2M'))
            $q .= "\t\"id\" ".$this->get_primary_type().",\n";

        $anns = get_class_vars ($class);
        foreach ($anns as $field => $ann)
        {
            if (!Base::is_annotation ($ann))
                continue;
            $q .= "\t\"".$field.'" '.$this->get_sql_type ($class, $field, $ann);
            if (in_array ('unique', $ann, true))
                $q .= ' UNIQUE';
            $q .= ",\n";
        }
        $q .= $this->table_extra ($class);
        if (is_subclass_of ($class, 'BaseM2M'))
            $q .= "\tPRIMARY KEY (\"from\", \"to\"),\n";
        $q = substr ($q, 0, -2);
        $q .= "\n\t)".$this->get_table_postfix();
        $res[] = $q;
        return $res;
    }

    public function
    release ()
    {
        $this->pdo = null;
    }

    protected function
    get_deps ($class)
    {
        $deps = array();
        $anns = get_class_vars ($class);
        foreach ($anns as $field => $ann)
        {
            if (@$ann[0] === 'Reference')
                $deps[] = $ann['to'];
        }
        return array_unique ($deps);
    }

    protected function
    implicit_classes ($class)
    {
        $anns = get_class_vars ($class);
        $res = array();
        foreach ($anns as $field => $ann)
        {
            if (@$ann[0] === 'Set')
                $res[] = Helper::get_m2m_class ($class, $ann['of'], $field);
        }
        return array_unique ($res);
    }

    public function
    setup ()
    {
        $schema = array();
        $pre_schema = array();
        $classes = get_declared_classes ();
        foreach (get_declared_classes () as $class)
        {
            $refl = new ReflectionClass ($class);
            if (!$refl->isAbstract () && $refl->isSubclassOf ('Model'))
            {
                $classes = array_merge ($classes, $this->implicit_classes ($class));
            }
        }
        foreach ($classes as $class)
        {
            $refl = new ReflectionClass ($class);
            if (!$refl->isAbstract () && $refl->isSubclassOf ('Base'))
            {
                $this->deps[$class] = $this->get_deps ($class);
            }
        }

        while (!empty ($this->deps))
        {
            $topclass = null;
            foreach ($this->deps as $class => $deps)
            {
                if (empty ($deps))
                {
                    $topclass = $class;
                    break;
                }
            }
            if (!$topclass)
                die ("Circular dependency\n");
            $schema = array_merge ($schema, $this->setup_schema ($topclass));
            $pre_schema = array_merge ($pre_schema, $this->setup_pre_schema ($topclass));
            unset ($this->deps[$topclass]);
            foreach ($this->deps as &$deps)
            {
                $t = array_search ($topclass, $deps);
                if (false === $t)
                    continue;
                unset ($deps[$t]);
            }
        }
        return array_merge ($pre_schema, $schema);
    }

    protected function
    get_table_postfix ()
    {
        return '';
    }
}
